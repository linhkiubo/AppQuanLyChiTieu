package com.example.duan1;


import android.app.DatePickerDialog;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.duan1.DAO.KhoanChiDAO;
import com.example.duan1.DAO.KhoanThuDAO;
import com.example.duan1.model.KhoanChi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.duan1.DAO.KhoanThuDAO.TABLE_KhoanThu;

public class ThongKeActivity extends AppCompatActivity {
    private TextView tvtTongThu;
    private TextView tvtTongChi;
    private TextView tvtConLai;
    private Button btnChonNgay;
    private EditText edtChonNgay;
    private RecyclerView recyclerView;
    public static List<KhoanChi> Chi = new ArrayList<>();
    KhoanChiDAO khoanChiDAO;
    KhoanThuDAO khoanThuDAO;
    public Calendar calendar;
    public SimpleDateFormat simpleDateFormat;
    String date;


    public void date() {
        calendar = Calendar.getInstance();
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                edtChonNgay.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
                edtChonNgay.setEnabled(false);
            }
        }, nam, thang, ngay);
        datePickerDialog.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_ke);
        tvtTongThu = (TextView) findViewById(R.id.tvtTongThu);
        tvtTongChi = (TextView) findViewById(R.id.tvtTongChi);
        tvtConLai = (TextView) findViewById(R.id.tvtConLai);
        btnChonNgay = (Button) findViewById(R.id.btnChonNgay);
        edtChonNgay = (EditText)findViewById(R.id.edtChonNgay);
        recyclerView = (RecyclerView) findViewById(R.id.viewList);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        khoanChiDAO = new KhoanChiDAO(this);
        khoanThuDAO = new KhoanThuDAO(this);
        double tongchi = khoanChiDAO.getChi() ;
        tvtTongChi.setText("Tổng Chi: "+tongchi);
        tvtTongThu.setText("Tổng Thu: "+khoanThuDAO.getThu());
        tvtConLai.setText("Còn Lại: " + (khoanThuDAO.getThu()-tongchi ));


        edtChonNgay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date();
                edtChonNgay.setEnabled(false);
            }
        });

        btnChonNgay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date = edtChonNgay.getText().toString().trim();
                 Chi = khoanChiDAO.getKhoanChi(date);
            }
        });
    }


}

